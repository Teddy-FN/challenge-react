import React, { Component } from 'react';
// import ToDoList from './challenge2/toDoList';
import './App.css';
import File from './challenge3/file';
import './challenge3/style.css';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { activity: 'Makan' },
        { activity: 'Ngegame' },
        { activity: 'Music' },
        { activity: 'Ngoding' },
        { activity: 'Tidur' },
        { activity: 'NgeYoutube' },
        { activity: 'Mandi' },
        { activity: 'Holiday' },
      ]
    }
  }

  render() {
    const TodoDatas = this.state.data;
    return (
      <div>
        <h1 style={{ textAlign: "center" }}>To Do List</h1>
        <div className='container'>
          {TodoDatas.map(content => {
            return (
              <File name={content.activity} />
            );
          })}
        </div>
      </div>
    )
  }
}

export default App;
