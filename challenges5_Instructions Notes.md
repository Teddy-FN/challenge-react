Todo App Phase 1
1. From scratch, initialize the React app
2. Render and Create the <App /> component from scratch
3. Have the <App /> component render 3 or 4 checkboxes with paragraphs or spans next to it (Using JSX input and p element)
4. like you're making a todo list with some hard-coded(ditulis di dalam props) items on it
5. Push to Gitlab
---------------------------

Todo App Phase 2
Time to have fun styling! But first things first: 
1. Change the input and p combo on the code to be a new component called <TodoItem />. <TodoItem /> (for now) will just have the same displayed data like before (every todo item is the same) hardcoded(ditulis di dalam kode) inside of it. (We'll learn soon how to make the TodoItem more flexible)
2. Style up the page however you want! You're welcome to use regular CSS (in the CSS file) or inline styles, or both!
----------------------------

#Todo App Phase 3
Let's practice props and mapping components on our todo list app!
I've created a js file with some todos data in it, which I'm imported into this file. (Normally this data would come from an API call, not a local file). 
Challenge: Using the array map method, render a child component for each todo item in the todosData array and pass the relevant data to it.

----------------------------

#Todo App Phase 4
In the previous iteration of this todo list app, we pulled in todos data from a JSON file and mapped over it to display the todo items.
Eventually we'll want to be able to modify the data, which will only happen if we've "loaded" the data in to the component's state
Challenge: Change the <App /> component into a stateful class component and load the imported `todosData` into state.
----------------------------

#Todo App Phase 5
 Challenge: Get rid of our warning about not having an onChange on our input. For now, the function that runs onChange can simply console.log something.
 ----------------------------

#Todo App Phase 6
 Challenge: 
  1. In the TodoItem component, make it so when the `onChange` event happens, it calls the `handleChange` method and passes the id of the todo into the function
  2. Update state so that the item with the given id flips `completed` from false to true (or vise versa)
     Remember not to modify prevState directly, but instead to return a new version of state with the change you want included in that update. (Think how you might use the `.map` method to do this)
 
  Let's make it so our checkbox can actually mark our todo as complete or incomplete!
  This challenge is a little more involved than some of the past ones. Check the comments in the code for some help on accomplishing this one
 
  Challenge: 
  1. Create an event handler in the App component for when the checkbox is clicked (which is an `onChange` event)
     a. This method will be the trickest part. Check the comments in the stubbed-out method below for some pseudocode to help guide you through this part
  2. Pass the method down to the TodoItem component
  3. In the TodoItem component, make it so when the `onChange` event happens, it calls the `handleChange` method and passes the id of the todo into the function
 ----------------------------

 /**Todo App Phase 7
 * Challenge: Style the completed todo items differently from the incomplete items.
 */